class Generator {
    private int[] key_1;
    private int[] key_2;
    private int[] key_3;

    Generator(int[] key_1, int[] key_2, int[] key_3) {
        this.key_1 = key_1;
        this.key_2 = key_2;
        this.key_3 = key_3;
    }

    int creat_elem() {
        Pair pair;
        int result = 1;
        boolean flag_1 = false, flag_2 = false, flag_3 = false;
        int result_1, result_2, result_3;
        if (transference(key_1[24], key_1[55]))//магические биты)))
            flag_1 = true;
        if (transference(key_2[7], key_2[57]))//магические биты)))
            flag_2 = true;
        if (transference(key_3[19], key_3[58]))//магические биты)))
            flag_3 = true;
        int mod = 999999999;
        if (flag_1 && flag_2 && flag_3 || !flag_1 && !flag_2 && !flag_3) {//если во всех трёх имеется бит переноса
            pair = shift(key_1, (key_1[24] + key_1[55]) % mod);
            key_1 = pair.get_massive();
            result_1 = pair.get_colum();
            pair = shift(key_2, key_2[7] + key_2[57] % mod);
            key_2 = pair.get_massive();
            result_2 = pair.get_colum();
            pair = shift(key_3, key_3[19] + key_3[58] % mod);
            key_3 = pair.get_massive();
            result_3 = pair.get_colum();
            return (result_1 ^ result_2) ^ result_3;
        }
        if (flag_1 && flag_2 || !flag_1 && !flag_2) {
            pair = shift(key_1, key_1[24] + key_1[55] % mod);
            key_1 = pair.get_massive();
            result_1 = pair.get_colum();
            pair = shift(key_2, key_2[7] + key_2[57] % mod);
            key_2 = pair.get_massive();
            result_2 = pair.get_colum();
            return result_1 ^ result_2;
        }
        if (flag_1 && flag_3 || !flag_1 && !flag_3) {
            pair = shift(key_1, key_1[24] + key_1[55] % mod);
            key_1 = pair.get_massive();
            result_1 = pair.get_colum();
            pair = shift(key_3, key_3[19] + key_3[58] % mod);
            key_3 = pair.get_massive();
            result_3 = pair.get_colum();
            return result_1 ^ result_3;
        }
        if (flag_2 && flag_3 || !flag_2 && !flag_3) {
            pair = shift(key_2, key_2[7] + key_2[57] % mod);
            key_2 = pair.get_massive();
            result_2 = pair.get_colum();
            pair = shift(key_3, key_3[19] + key_3[58] % mod);
            key_3 = pair.get_massive();
            result_3 = pair.get_colum();
            return result_2 ^ result_3;
        }
        return result;
    }



    private Pair shift(int[] massive, int value) {
        Pair para = new Pair();
        int last = 0;
        for (int index = 0; index < massive.length - 1; index++) {
            last = massive[index + 1];
            massive[index + 1] = massive[index];
        }
        massive[0] = value;
        para.add_colum(last);
        para.add_massive(massive);
        return para;
    }
}