public class LFSR {

    private static int[] reg1 = new int[55]; // sum takes 24|55
    private static int[] reg2 = new int[57]; // sum takes  7|57
    private static int[] reg3 = new int[58]; // sum takes 19|58
    private static boolean cro1, cro2, cro3;

    public static void makeSeed(){
        for(int i = 0; i < 55; i++) reg1[i] = (int)System.currentTimeMillis() * (int)(Math.random()*10);
        for(int i = 0; i < 57; i++) reg2[i] = (int)System.currentTimeMillis() * (int)(Math.random()*10);
        for(int i = 0; i < 58; i++) reg3[i] = (int)System.currentTimeMillis() * (int)(Math.random()*10);
    }

    // do we mean jump if there's one, or me xor each of them&

    private static boolean transference(int value_1, int value_2) {
        long result = (long) value_1 + (long) value_2;
        result = result >>> 32;
        return (result & 1) == 1;
    }

    public static void main(String[] args){
        cro1 = cro2 = cro3 = false;
        makeSeed();
        if(transference(reg1[24], reg1[55])) cro1 = true;
        if(transference(reg2[7],  reg2[57])) cro2 = true;
        if(transference(reg3[19], reg3[58])) cro3 = true;
        //
        // case 1
        if(cro1 && cro2 && cro3){ // each one strikes

        }
        if(cro1 && cro2 && !cro3){ // only 2

        }
        if(cro1 && cro3 && !cro2){ // only 2

        }
        if(!cro1 && cro2 && cro3){ // only 2

        }
    }
}

//public class LFdSR {

//    public static void main(String[] args) {
//        // initial fill
//        boolean[] a = {
//                false, true, false, false, false,
//                false, true, false, true, true, false
//        };
//        int trials = Integer.parseInt(args[0]);    // number of steps
//        int n = a.length;                          // length of register
//        int TAP = 8;                               // tap position
        // Simulate operation of shift register.
//        for (int t = 0; t < trials; t++) {
            // Simulate one shift-register step.
//            boolean next = (a[n-1] ^ a[TAP]);  // Compute next bit.
//            for (int i = n-1; i > 0; i--)
//                a[i] = a[i-1];                  // Shift one position.
//            a[0] = next;                       // Put next bit on right end.
//            if (next) System.out.print("1");
//            else      System.out.print("0");
//        }
//        System.out.println();
//    }
////}